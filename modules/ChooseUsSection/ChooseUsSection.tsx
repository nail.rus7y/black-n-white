import React from "react";
import Image from "next/image";
import styles from "../Header/Header.module.css";
import {Element} from "react-scroll";

export default function ChooseUsSection() {
	return (
		<Element name={"about"}>
			<section>
				<div className={"uppercase text-3xl bg-black text-white text-center py-10"}>
					<h2>Почему выбирают нас?</h2>
				</div>
				<div className={"uppercase text-center my-14"}>
					<div className={"m-auto font-light md:text-lg"}>
						<p>В рамках <span className={"font-normal"}>detailingdom</span> работают такие компании как</p>
						<br />
						<p><span className={"font-normal"}>black&white</span> - полировка кузова, защитные покрытия, бронирование кузова, химчистка салона</p>
						<p><span className={"font-normal"}>lableather</span> - ремонт и покраска кожи</p>
						<p><span className={"font-normal"}>#carsformers</span> - удаление вмятин без покраски</p>
					</div>
				</div>
				<div className={"text-center"}>
					<h3 className={"uppercase text-3xl mt-24 mb-8"}>Наши преимущества</h3>
					<div className={"flex justify-around my-10 flex-col lg:flex-row font-light md:text-lg"}>
						<div className={"basis-full py-10"}>
							<Image src={require("../../public/img/choose/work.png")} alt={"Высококвалифицированные специалисты"} width={"100px"} height={"100px"}/>
							<p className={"mt-4 px-10"}>Работу выполняют высококвалифицированные специалисты с большим опытом.</p>
						</div>
						<div className={"basis-full py-10"}>
							<Image src={require("../../public/img/choose/price.png")} alt={"Лучшие цены на рынке"} width={"100px"} height={"100px"}/>
							<p className={"mt-4 px-10"}>Мы сохраняем лучшие цены на рынке в данном сегменте качественного обслуживания.</p>
						</div>
						<div className={"basis-full py-10"}>
							<Image src={require("../../public/img/choose/tech.png")} alt={"Европейские технологии"} width={"100px"} height={"100px"}/>
							<p className={"mt-4 px-10"}>Мы применяем европейские технологии.</p>
						</div>
					</div>
				</div>
				<div className={"text-center mt-20 mb-10"}>
					<button
						onClick={() => window.open("https://api.whatsapp.com/send?phone=79213390024", "_blank")}
						style={{
							background: "linear-gradient(100deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0) 90%, rgba(255, 184, 0,0.7) 91%, rgba(255, 184, 0,0.7) 96%, rgba(255, 184, 0,0) 97%)",
							backgroundSize: "200% 200%",
							borderColor: "rgba(255, 184, 0, 1)",
							borderWidth: "3px"
						}}
						className={styles.btn + " tracking-widest md:text-xl text-md font-thin opacity-80 rounded-full py-3 px-14 hover:opacity-100 hover:border-opacity-100"}
						type={"button"}
					>
						ЗАПИСАТЬСЯ
					</button>
				</div>
				<div className={"bg-gray-100"}>
					<div
						style={{
							height: "100px",
							background: "white",
							clipPath: "polygon(0% 0%, 50% 100%, 100% 0%)",
						}}
					/>
				</div>
				<div className={"uppercase text-xl md:text-4xl bg-gray-100 py-52 text-center"}>
					<p className={"w-4/5 sm:w-2/5 mx-auto"}>Приезжайте сегодня к нам и почувствуйте разницу!</p>
				</div>
			</section>
		</Element>
	)
}