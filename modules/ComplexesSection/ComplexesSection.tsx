import React, {useEffect, useState} from "react";
import {CarouselProvider, Slide, Slider} from "pure-react-carousel";
import {useMediaQuery} from "react-responsive";
import 'pure-react-carousel/dist/react-carousel.es.css';
import Modal from "react-modal";
import Carousel, {ResponsiveType} from 'react-multi-carousel';
// @ts-ignore
import CloseIcon from "../close.svg";

enum ComplexesEnum {
  none,
  season,
  glass,
  ceramic,
  optimal,
  ultimate
}

export default function ComplexesSection() {
  const isDesktop  = useMediaQuery({
    minWidth: 1400
  })
  const isTablet = useMediaQuery({
    minWidth: 1000
  })
  const isMobile = useMediaQuery({
    minWidth: 600
  })
  const [modal, setModal] = useState<ComplexesEnum>(ComplexesEnum.none)
  const visibleSlides = isDesktop ? 4 : isTablet ? 3 : isMobile ? 2 : 1;

  const onClose = () => {
    setModal(ComplexesEnum.none)
  }

  return (
    <>
      <div className={"bg-gray-100 pb-10"}>
        <h2 className={"text-4xl py-8 sm:mx-12 px-12 border-b border-black font-light uppercase"}>Готовые комплексы</h2>
        <div className={"p-10 sm:text-xl text-center"}>
          <p className={"sm:px-28 pt-0"}>
            Мы высоко ценим время наших клиентов и поэтому подготовили для вас несколько готовых предложений, комплексных программ, руководствуясь наиболее частыми пожеланиями владельцев автомобилей.
          </p>
          <p className={"pb-16 sm:px-28"}>
            Готовые пакеты услуг позволяют вам не только <span className={"font-bold"}>сэкономить (в среднем около 15%)</span>, но и избавиться от мучительного выбора.
            Наши специалисты охотно проконсультируют вас и предложат программу, наиболее подходящую именно вашему автомобилю
          </p>
        </div>
      <RenderCarousel visibleSlides={visibleSlides} setModal={(complex: ComplexesEnum) => setModal(complex)} />
      </div>
      {openModal({ type: modal, onClose, isTabletOrMobile: isTablet })}
    </>
  )
}

function RenderCarousel({ visibleSlides, setModal }: { visibleSlides: number; setModal: Function }) {
  const responsive: ResponsiveType = {
    desktop: {
      breakpoint: { min: 1400, max: 3000 },
      items: 4
    },
    laptop: {
      breakpoint: { max: 1400, min: 1000 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1000, min: 600 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 600, min: 0 },
      items: 1
    }
  }
  return (
    <Carousel
      responsive={responsive}
      infinite
    >
      <img onClick={() => setModal(ComplexesEnum.season)} className={"mx-auto cursor-pointer h-fit"} alt={"season"} src={require(`../../public/img/complexes/season.png`)} />
      <img onClick={() => setModal(ComplexesEnum.glass)} className={"mx-auto cursor-pointer h-fit"} alt={"glass"} src={require(`../../public/img/complexes/glass.png`)} />
      <img onClick={() => setModal(ComplexesEnum.ceramic)} className={"mx-auto cursor-pointer h-fit"} alt={"ceramic"} src={require(`../../public/img/complexes/ceramic.png`)} />
      <img onClick={() => setModal(ComplexesEnum.optimal)} className={"mx-auto cursor-pointer h-fit"} alt={"optimal"} src={require(`../../public/img/complexes/optimal.png`)} />
      <img onClick={() => setModal(ComplexesEnum.ultimate)} className={"mx-auto cursor-pointer h-fit"} alt={"ultimate"} src={require(`../../public/img/complexes/ultimate.png`)} />
    </Carousel>
  )
}

function openModal({ type, onClose, isTabletOrMobile }: { type: ComplexesEnum, onClose: () => void, isTabletOrMobile: boolean }) {
  if (type === ComplexesEnum.none) {
    return null;
  }

  function renderModal() {
    switch (type) {
      case ComplexesEnum.season:
        return (
          <>
            <div className={"flex flex-col"}>
              <div className={"bg-black py-8"}>
                <div className={"flex justify-center border-b-2 border-b-yellow-400 mx-20"}>
                  <h3 className={"text-2xl font-bold text-white py-4"}>SEASON</h3>
                </div>
              </div>
              <div className={"flex-grow bg-gray-100 px-10 py-14 text-xl"}>
                <p className={"mb-6"}>Комплекс "Season" - программа сезонной защиты кузова.</p>
                <p className={"mb-6"}>Преимущества - внешний вид, защита от осадков и реагентов, цена.</p>
                <p>Периодичность обновления 3 - 5 месяцев.</p>
              </div>
            </div>
            {isTabletOrMobile && <div className={"flex-grow bg-gray-100"}>
              <img className={"block object-cover"} src={require("../../public/img/complexes/modal/season.png")} />
            </div>}
          </>
        )
      case ComplexesEnum.glass:
        return (
          <>
            <div className={"flex flex-col"}>
              <div className={"bg-black py-8"}>
                <div className={"flex justify-center border-b-2 border-b-yellow-400 mx-20"}>
                  <h3 className={"text-2xl font-bold text-white py-4"}>GLASS GUARD</h3>
                </div>
              </div>
              <div className={"flex-grow bg-gray-100 px-10 py-14 text-xl"}>
                <p className={"mb-6"}>Комплекс "GlassGuard" - программа защиты кузова.</p>
                <p className={"mb-6"}>Преимущества - внешний вид, защита от осадков и реагентов, цена, увеличенный срок эксплуатации.</p>
                <p>Периодичность обновления 8 - 12 месяцев.</p>
              </div>
            </div>
            {isTabletOrMobile && <div className={"flex-grow bg-gray-100"}>
              <img className={"block object-cover"} src={require("../../public/img/complexes/modal/glass.png")} />
            </div>}
          </>
        )
      case ComplexesEnum.ceramic:
        return (
          <>
            <div className={"flex flex-col"}>
              <div className={"bg-black py-8"}>
                <div className={"flex justify-center border-b-2 border-b-yellow-400 mx-20"}>
                  <h3 className={"text-2xl font-bold text-white py-4"}>CERAMIC</h3>
                </div>
              </div>
              <div className={"flex-grow bg-gray-100 px-10 py-14 text-xl"}>
                <p className={"mb-6"}>Комплекс "Ceramic" - это программа защиты кузова.</p>
                <p className={"mb-6"}>Преимущества - внешний вид, защита от осадков, реагентов, мелких царапин и сколов, увеличенный срок эксплуатации.</p>
                <p>Периодичность обновления 12 - 18 месяцев.</p>
              </div>
            </div>
            {isTabletOrMobile && <div className={"flex-grow bg-gray-100"}>
              <img className={"block object-cover"} src={require("../../public/img/complexes/modal/ceramic.png")} />
            </div>}
          </>
        )
      case ComplexesEnum.optimal:
        return (
          <>
            <div className={"flex flex-col"}>
              <div className={"bg-black py-8"}>
                <div className={"flex justify-center border-b-2 border-b-yellow-400 mx-20"}>
                  <h3 className={"text-2xl font-bold text-white py-4"}>THE OPTIMAL</h3>
                </div>
              </div>
              <div className={"flex-grow bg-gray-100 px-10 py-14 text-xl"}>
                <p className={"mb-6"}>Комплекс "The Optimal" - программа ухода и защиты кузов и салона автомобиля.</p>
                <p className={"mb-6"}>Преимущества - внешний вид, чистота и комфорт салона, защита от осадков, реагентов, мелких царапин и сколов, увеличенный срок эксплуатации.</p>
                <p>Периодичность обновления 12 - 18 месяцев.</p>
              </div>
            </div>
            {isTabletOrMobile && <div className={"flex-grow bg-gray-100"}>
              <img className={"block object-cover"} src={require("../../public/img/complexes/modal/optimal.png")} />
            </div>}
          </>
        )
      case ComplexesEnum.ultimate:
        return (
          <>
            <div className={"flex flex-col max-h-full"}>
              <div className={"bg-black py-8"}>
                <div className={"flex justify-center border-b-2 border-b-yellow-400 mx-20"}>
                  <h3 className={"text-2xl font-bold text-white py-4"}>THE ULTIMATE</h3>
                </div>
              </div>
              <div className={"flex-grow bg-gray-100 px-10 py-14 text-xl"}>
                <p className={"mb-6"}>Комплекс "The Ultimate" - программа ухода и максимальной защиты кузова и салона автомобиля.</p>
                <p className={"mb-6"}>Преимущества - внешний вид, максимальная защита от осадков, реагентов, царапин и сколов, чистота и комфорт салона, длительный срок эксплуатации.</p>
                <p>Периодичность обновления 12 - 18 месяцев.</p>
              </div>
            </div>
            {isTabletOrMobile && <div className={"flex-grow bg-gray-100"}>
              <img className={"block object-cover"} src={require("../../public/img/complexes/modal/ultimate.png")} />
            </div>}
          </>
        )
    }
  }

  return (
    <Modal style={{ content:
      {
        padding: 0,
        minHeight: "fit-content",
        maxHeight: "95vh",
        maxWidth: "95%",
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        overflow: "hidden"
      }
    }} isOpen={!!type}>
      <div className={"flex relative"}>
        <div onClick={onClose} className={"absolute right-0 top-0 m-2 hover:opacity-80 cursor-pointer"}>
          <CloseIcon />
        </div>
        {renderModal()}
      </div>
    </Modal>
  )

}