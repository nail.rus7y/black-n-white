import {ButtonBack, ButtonNext, CarouselProvider, DotGroup, Slide, Slider} from 'pure-react-carousel';
import {useMediaQuery} from "react-responsive";
import {LeftArrow, RightArrow} from "../../components/icons";

export default function FeedbackCarousel() {
  const isTabletOrMobile = useMediaQuery({ query: '(min-device-width: 1224px)' })
  const isMobile = useMediaQuery({ query: '(min-device-width: 600px)' })

  return (
    <div className={"flex-grow flex items-center"}>
      <div className={"h-3/5 w-fit mx-auto"}>
        <CarouselProvider
          className={"flex h-full"}
          naturalSlideWidth={100}
          naturalSlideHeight={100}
          isIntrinsicHeight
          visibleSlides={isTabletOrMobile ? 1 : 2}
          totalSlides={3}
        >
          <ButtonBack className={isMobile ? "" : "relative -mr-8 z-20"}><LeftArrow /></ButtonBack>
          <Slider className={"h-full flex items-center"}>
            <Slide index={0}>
              <img className={"mx-auto"} alt={"feedback1"} src={require("../../../public/img/feedback/feedback1.png")} />
            </Slide>
            <Slide index={1}>
              <img className={"mx-auto"} alt={"feedback2"} src={require("../../../public/img/feedback/feedback2.png")} />
            </Slide>
            <Slide index={2}>
              <img className={"mx-auto"} alt={"feedback3"} src={require("../../../public/img/feedback/feedback3.png")} />
            </Slide>
          </Slider>
          <ButtonNext className={isMobile ? "" : "relative -ml-8 z-20"}><RightArrow /></ButtonNext>
        </CarouselProvider>
      </div>
    </div>
  )
}