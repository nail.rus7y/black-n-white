import { FeedbackCarousel } from "./FeedbackCarousel";
import {Element} from "react-scroll";

export default function FeedbackSection() {
  return (
    <Element name={"feedbacks"}>
      <section className={"flex flex-col h-screen bg-gray-100"}>
        <h2 className={"text-4xl sm:mx-12 width-full pb-8 pt-40 px-12 border-b border-black font-light uppercase"} style={{ boxShadow: "0px 1px 0px 0px rgba(34, 60, 80, 0.2)" }}>
          Отзывы наших клиентов
        </h2>
        <FeedbackCarousel />
      </section>
    </Element>
  )
}