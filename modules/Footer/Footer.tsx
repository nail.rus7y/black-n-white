import { FooterContacts } from "./FooterContacts";
import { FooterAdditionalInfo } from "./FooterAdditionalInfo";
import FooterMap from "./FooterMap/FooterMap";

export default function Footer() {
  return (
    <footer className={"flex flex-col bg-black h-screen"}>
      <FooterContacts />
      <FooterAdditionalInfo />
      <FooterMap />
    </footer>
  )
}