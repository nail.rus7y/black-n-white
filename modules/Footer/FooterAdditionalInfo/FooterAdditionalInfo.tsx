export default function FooterAdditionalInfo() {
  return (
    <section className={"flex pb-10"}>
      <div className={"w-1/2 lg:w-1/3 m-auto sm:text-base text-sm text-white text-center tracking-wide"}>
        <p className={"pb-2"}>Часы работы:</p>
        <p>ПН-ПТ 09:00-20:00</p>
        <p>СБ 09:00-18:00</p>
      </div>
    </section>
  )
}