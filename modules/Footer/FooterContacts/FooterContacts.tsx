import {Element} from "react-scroll";

export default function FooterContacts() {
  return (
    <Element name={"contacts"} >
      <section className={"flex-col text-lg text-white tracking-wider font-bold text-center leading-8 py-10"}>
        <p>Саперная ул., 67В, Пушкин</p>
        <p>+7(812)679-40-24</p>
      </section>
    </Element>
  )
}