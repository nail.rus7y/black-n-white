export default function FooterMap() {
  return (
    <section className={"flex flex-1"}>
      <iframe
        className={"min-w-full"}
        style={{ border: 0 }}
        loading="lazy"
        allowFullScreen
        src="https://www.google.com/maps/embed/v1/place?language=ru&q=place_id:ChIJ68xakKAhlkYRsWtOW5wMAYw&key=AIzaSyBScI4kvxkDjJDwro0-S1NN4FhtijktLuY">
      </iframe>
    </section>
  )
}