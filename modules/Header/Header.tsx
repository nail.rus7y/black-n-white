import { navLinks } from "../../pages";
import { useMediaQuery } from "react-responsive";
import {Link} from "react-scroll";

export default function Header() {
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1060px)' })

  return (
    <header className={"bg-black sm:px-12 py-8"}>
      <nav className={"flex items-center justify-between pl-12 sm:px-12 pb-8 md:border-b md:border-white"}>
        {isTabletOrMobile
          ? null
          : (
            <div className={"flex space-x-8"}>
              {navLinks.map(({ to, name }) =>
                <span key={name} className={"text-white font-sans cursor-pointer opacity-80 hover:opacity-100"}>
                  <Link to={to} smooth={true} duration={1000}>
                    {name}
                  </Link>
                </span>
              )}
            </div>
          )
        }
        <section className={"flex ml-auto text-white items-center w-fit gap-3"}>
          <span className={"block font-bold tracking-wider leading-8 mr-8 sm:ml-2"}>+7(812)679-40-24</span>
          <img className={"sm:mr-0 mr-8 h-full"} width={32} height={32} alt={"Телефон компании"} src={require("../../public/img/phone.png")} />
        </section>
      </nav>
    </header>
  );
}