import React, {useState} from "react";
import {PriceButton} from "../components/PriceButton";
import {Element} from "react-scroll";

const priceTables = {
	interior: [
		["Химчистка", "12 000 р.", "15 000 р.", "17 000 р."],
		["Покраска кожи", "4 000 р.", "4 000 р.", "4 000 р."],
		["Шумоизоляция", "30 000 р.", "40 000 р.", "60 000 р."],
		["Защита салона", "5 000 р.", "6 000 р.", "8 000 р."],
	],
	body: [
		["Полировка кузова", "15 000 р.", "18 000 р.", "23 000 р."],
		["Удаление вмятин", "от 3 000 р.", "от 3 000 р.", "от 3 000 р."],
		["Защитные покрытия", "10 000 р.", "15 000 р.", "20 000 р."],
		["Оклейка пленкой", "5 000 р.", "6 000 р.", "8 000 р."],
		["Химчистка подвески", "1 000 р.", "1 000 р.", "1 000 р."],
		["Детейлинг мойка", "1 000 р.", "1 000 р.", "1 000 р."],
	],
	optics: [
		["Полировка стекла", "4 000 р.", "4 000 р.", "4 000 р."],
		["Полировка фар", "1 200 р.", "1 400 р.", "1 600 р."],
		["Бронирование стекла", "4 000 р.", "4 000 р.", "5 000 р."],
		["Полировка фонарей", "1 000 р.", "1 000 р.", "1 000 р."],
	],
	complex: [
		['Комплекс "SEASON"', "8 000 р.", "10 000 р.", "12 000 р."],
		['Комплекс "GLASS GUARD"', "23 000 р.", "30 000 р.", "40 000 р."],
		['Комплекс "CERAMIC"', "33 000 р.", "40 000 р.", "50 000 р."],
		['Комплекс "THE OPTIMAL"', "58 000 р.", "65 000 р.", "75 000 р."],
		['Комплекс "THE ULTIMATE"', "100 000 р.", "115 000 р.", "125 000 р."],
	],
}

export default function PriceSection() {
	const [currentTable, setCurrentTable] = useState<keyof typeof priceTables>("interior");

	return (
		<Element name={"prices"}>
			<section className={"bg-gray-100"}>
				<h2 className={"text-4xl py-8 sm:mx-12 px-12 border-b border-black font-light uppercase"}>Цены</h2>
				<div className={"overflow-auto mb-32"}>
					<table className="mx-auto mt-10 text-center font-light text-base md:text-xl">
						<tbody>
							<tr>
								<td className={"py-10 w-2/4 text-left"} />
								<td className={"px-6 py-14"}>1 класс</td>
								<td className={"px-6 py-14"}>2 класс</td>
								<td className={"px-6 py-14"}>3 класс</td>
							</tr>
							{priceTables[currentTable].map((row) => {
								return (
									<tr key={row[0]}>
										{row.map((cell, i) => <td key={cell + i} className={`px-6 py-4 ${i ? "" : "text-left"}`}>{cell}</td>)}
									</tr>
								)
							})}
						</tbody>
					</table>
				</div>
				<div className={"flex sm:flex-row sm:justify-center sm:gap-0 flex-col items-center gap-3"}>
					<PriceButton onClick={() => setCurrentTable("interior")} title={"Салон"} disabled={currentTable === "interior"} />
					<PriceButton onClick={() => setCurrentTable("body")} title={"Кузов"} disabled={currentTable === "body"} />
					<PriceButton onClick={() => setCurrentTable("optics")} title={"Стекла и оптика"} disabled={currentTable === "optics"} />
					<PriceButton onClick={() => setCurrentTable("complex")} title={"Комплексы"} disabled={currentTable === "complex"} />
				</div>
			</section>
		</Element>
	)
}

