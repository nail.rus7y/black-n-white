import React, {useState} from "react";
import styles from "../ServicesSection.module.css";

export default function Service({ className, name, options }: { className: string, name: string, options: { option: string }[] }) {
  const [focused, setFocused] = useState(false);

  return (
    <div onMouseEnter={() => setFocused(true)} onMouseLeave={() => setFocused(false)} className={`${styles.btn} ${className} overflow-hidden`}>
      <ul className={`transition-all text-white absolute list-disc mt-6 ml-12 ${focused ? styles.mFadeIn : styles.mFadeOut}`}>
        {options.map(({ option }) => {
          return (
            <li key={option} className={"hover:font-bold"}>
              <a>{option}</a>
            </li>
          );
        })}
      </ul>
      <div className={`transition-all absolute text-white ${focused ? styles.mFadeOut : styles.mFadeIn}`}>
        <div className={"mx-6 h-20 flex items-center w-20 inline-block font-bold border-b-yellow-400 border-b-4 uppercase"}>
          {name}
        </div>
      </div>
    </div>
  )
}