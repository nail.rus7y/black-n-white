import React from "react";
import styles from "./ServicesSection.module.css";
import {Service} from "./Service";
import {Element} from "react-scroll";

export default function ServicesSection() {
	return (
    <Element name={"services"}>
      <section className={"bg-gray-100"}>
        <h2 className={"text-4xl py-8 sm:mx-12 px-12 border-b border-black font-light uppercase"}>Наши услуги</h2>
        <div className={"flex min-w-min max-w-4xl lg:max-w-3xl sm:max-w-fit flex-wrap mx-auto justify-center py-20"}>
          <Service
            className={styles.body}
            name={"Кузов"}
            options={[{ option: "Удаление вмятин" }, { option: "Защитные покрытия" }, { option: "Защитные пленки" }, { option: "Химчистка подвески" }, { option: "Полировка кузова" }]}
          />
          <Service
            className={styles.interior}
            name={"Салон"}
            options={[{ option: "Шумоизоляция салона" }, { option: "Химчистка салона" }, { option: "Защита салона" }, { option: "Покраска кожи" }]}
          />
          <Service className={styles.carwash} name={"Мойка"} options={[{ option: "Детейлинг мойка" }]} />
          <Service className={styles.optics} name={"Стекло и оптика"} options={[{ option: "Полировка стекол" }, { option: "Полировка фар" }]} />
        </div>
      </section>
    </Element>
	)
}

