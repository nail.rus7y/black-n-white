import React from "react";

interface PriceButtonProps {
	onClick: () => void;
	title: string;
	disabled: boolean;
}

export default function PriceButton({ onClick, disabled, title }: PriceButtonProps) {
	return (
		<button
			disabled={disabled}
			className={`mx-1 ${disabled ? "bg-gray-400" : "bg-black hover:bg-gray-600"} bg-black text-white py-5 w-40 rounded active:bg-gray-700`}
			onClick={onClick}
		>
			{title}
		</button>
	)
}