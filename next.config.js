/** @type {import('next').NextConfig} */
const withImages = require('next-images')
module.exports = withImages({
  fileExtensions: ["jpg", "jpeg", "png", "gif"],
  images: {
    disableStaticImages: true
  },
  webpack(config, options) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        // for webpack 5 use
        and: [/\.(js|ts)x?$/]
      },

      use: ['@svgr/webpack'],
    });

    return config
  }
})
