import type { AppProps } from 'next/app'
import 'tailwindcss/tailwind.css'
import "./burger.css"
import 'react-multi-carousel/lib/styles.css';

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
