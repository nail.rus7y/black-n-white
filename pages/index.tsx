import type {NextPage} from 'next'
import Head from 'next/head'
import {Header} from "../modules/Header";
import {Footer} from "../modules/Footer";
import styles from "../modules/Header/Header.module.css";
import {FeedbackSection} from "../modules/FeedbackSection";
import 'pure-react-carousel/dist/react-carousel.es.css';
import {slide as Menu} from "react-burger-menu";
import {useMediaQuery} from "react-responsive";
import ChooseUsSection from "../modules/ChooseUsSection/ChooseUsSection";
import ServicesSection from "../modules/ServicesSection/ServicesSection";
import PriceSection from "../modules/PriceSection/PriceSection";
import {ComplexesSection} from "../modules/ComplexesSection";
import { Link } from "react-scroll";

export const navLinks = [
  { name: "КОНТАКТЫ", to: "contacts" },
  { name: "О НАС", to: "about" },
  { name: "НАШИ УСЛУГИ", to: "services" },
  // { name: "ПОРТФОЛИО", to: "portfolio" },
  { name: "ЦЕНЫ", to: "prices" },
  { name: "ОТЗЫВЫ", to: "feedbacks" },
];

const Home: NextPage = () => {
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1024px)' })

  return (
    <div id={"outer-container"} className={"min-h-screen w-full"}>
      {isTabletOrMobile ? <Menu>
        {navLinks.map(({ to, name }) =>
          <li className="menu-ite text-white font-sans cursor-pointer opacity-80 hover:opacity-100" key={name}>
            <Link to={to} smooth={true} duration={800}>{name}</Link>
          </li>
        )}
      </Menu> : null}
      <Head>
        <title>Black&White - detailing service</title>
        <meta name="description" content="Black&White" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="keywords" content="Профессиональный детейлинг сервис Пушкин, Центр профессионального детейлинга, Санкт-Петербург детейлинг сервис, Полировка авто город Пушкин, Удаление вмятин, Профессиональная полировка" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
	  	<link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin={"use-credentials"} />
	  	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet" />
      </Head>
      <Header />
      <main className={"flex flex-col w-full"}>
        <section style={{ backgroundImage: "url(" + `${require("../public/img/car.png")}` + ")" }} className={"w-full h-screen bg-cover bg-no-repeat bg-bottom flex"}>
          <div className={"sm:mt-40 sm:ml-40 mt-28 ml-14"}>
            <h1 className={"sm:text-5xl lg:text-6xl text-2xl text-white font-bold mb-10"}>DETAILING<span className={"text-yellow-400"}>DOM</span></h1>
            <p className={"lg:text-4xl sm:text-2xl text-xl text-white sm:w-3/4 w-4/6 mb-2"}>
              Детейлинг сервис в г.&#160;Пушкин.
            </p>
            <p className={"lg:text-4xl sm:text-2xl text-xl text-white sm:w-3/4 w-5/6 sm:mb-14 mb-56"}>
              Эстетика и комфорт вашего автомобиля.
            </p>
            <button
              onClick={() => window.open("https://api.whatsapp.com/send?phone=79213390024", "_blank")}
              style={{
                background: "linear-gradient(100deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0) 90%, rgba(255,255,255,0.7) 91%, rgba(255,255,255,0.7) 96%, rgba(0,0,0,0) 97%)",
                backgroundSize: "200% 200%",
			    borderWidth: "3px"
              }}
              className={styles.btn + " tracking-widest md:text-xl text-md font-thin text-white opacity-80 border-white border-2 rounded-full py-3 px-14 hover:opacity-100 hover:border-opacity-100"}
              type={"button"}
            >
              ЗАПИСАТЬСЯ
            </button>
          </div>
        </section>
        <ServicesSection />
        <ComplexesSection />
        <ChooseUsSection />
        <PriceSection />
        <FeedbackSection />
      </main>

      <Footer />
    </div>
  )
}

export default Home
