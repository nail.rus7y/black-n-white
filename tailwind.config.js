const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', "./modules/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: [
          'Roboto',
          ...defaultTheme.fontFamily.sans,
        ]
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
